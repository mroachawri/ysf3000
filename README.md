### Yeast Strain Explorer 3000

A Shiny app to explore a yeast phylogeny and integrate phenotypic data.

### Dependencies

R packages
- shiny
- shinythemes
- ggtree
- treeio
- dplyr
- ggplot2
- xlsx
- DT
- ggbiplot

### Run

Instructions are on the home screen.

```R
library(shiny)
runUrl('https://bitbucket.org/mroachawri/ysf3000/get/master.zip')
```
